const router = require("koa-router")();
const { PassThrough } = require("stream");
const chat = require("@src/services/feed/chat");
const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

router.post("/chat-sse", async ({ request, response }) => {
  const body = request.fields || {};
  const messages = body.messages || [];
  const question = body.question || "";
  const userId = body.userId || "1";

  const stream = new PassThrough();
  // 设置响应头 response
  response.type = "text/event-stream";
  response.set("Cache-Control", "no-cache");
  response.set("Connection", "keep-alive");

  const onTokenStream = (token) => {
    // base64 编码
    const encoded = Buffer.from(token).toString("base64");
    stream.write("event: message\n");
    stream.write(`data: ${encoded}\n\n`);
  };

  chat(question, messages, userId, onTokenStream).then(result => {
    stream.end();
  })

  return response.success(info);
});

module.exports = exports = router.routes();
