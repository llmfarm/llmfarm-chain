const router = require("koa-router")();

router.prefix("/api/feed");

try { router.use(require(`./chat.js`)) } catch (error) { console.log(`load error`, error); }
try { router.use(require(`./chat-sse.js`)) } catch (error) { console.log(`load error`, error); }

module.exports = exports = router.routes();