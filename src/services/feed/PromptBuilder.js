class PromptBuilder {
  constructor() {

  }

  async build(content) {
    return `Thinking Step By Step! Think like God!
1. Please understand the dialogue context first
2. analyze which intention the user's intention best matches according to the provided intention options
3. return json of intent info, param's value needs to be confirmed mentioned in the conversation
!!! PAY ATTENTION: Provide Your Output ONLY in json format with the keys: intent, params

====== INTENT LIST:

1. 查询<作物>的<营养指标>含量, 需要调用 API (查询原料的营养参数)
intent: search_nutrition
param | 是否必填 | 数据类型 | 示例 | 说明
materialName | optional | Array | 玉米、大麦
nutrientName | optional | Array | 粗蛋白、钙、氯化钠

2. 当提问制作猪饲料时（仅限猪）, 查询[猪种类], [体重]/[胎次]的营养需求量
intent: search_required_nutrition
param | 是否必填 | 数据类型 | 示例 | 说明
stageName | optional | string | 可选项: 仔猪、生长育肥猪、后备母猪、种公猪、妊娠母猪、泌乳母猪、null | (猪的生长阶段, 通过用户回答获取)
weight | optional | number | 40 | 体重
childbirth | optional | number | 2  | 分娩次数

3. 意图: 根据上文中提及的营养量需求, 调整配方含量
intent: adjust_nutrition
param | 是否必填 | 数据类型 | 示例 | 说明
nutrientName | optional | Array<string> | 营养素名称 
value | optional | Array<number> | 1.1、-0.5 | 调整的百分比值, 1, -1
判断: 需要判断是否已经获取到 <营养量需求> 信息, 如果没有, 则执行 search_required_nutrition 的相关处理流程, 问询相关参数
response: json

4. 意图: 不需要调整并确认制作饲料配方
上下文场景: 查询并回复了营养量需求 & 确定不进行调整 | 不需要调整
intent: confirm_make_feed

5. 意图: 客气的话语、感谢等
示例: 好吧 | 那好 | 谢谢 | 没事儿了等类似语句
intent: polite

6. 意图: 询问如牛饲料、鸡饲料、天气等 与 猪饲料&营养量需求 无关的意图
intent: other

======

请仔细理解上述规则和需求
如果理解, 请回复 """我已理解需求, 请开始提问"""`
  }
}

module.exports = exports = PromptBuilder;