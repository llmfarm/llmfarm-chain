const httpInvoke = require('../common/invoke')

const rules = {
  'materialName': {
    required: true,
    message: (options) => {
      const message = `好的，请问您需要查询哪种原料的哪种营养素含量？
      
示例：
玉米的蛋白质含量是多少？`
      return message
    }
  },
  'nutrientName': {
    required: true,
    message: (options) => {
      const crop = options['materialName']
      const message = `好的，请问您需要查询${crop}的哪种营养素含量？
      
示例：
玉米的蛋白质含量是多少？`
      return message
    }
  }
}

const validateOptions = (options, rules) => {
  for (const key in rules) {
    const rule = rules[key];
    if (rule.required && !options[key]) {
      if (typeof rule.message === 'function') {
        return rule.message(options)
      }
      return rule.message || `请告诉我 ${key} 的相关信息`;
    }
  }
}

const resolveRequestOptions = (options) => {
  const { materialName, nutrientName } = options;
  const list = []
  for (const name of materialName) {
    for (const nutrient of nutrientName) {
      list.push({
        materialName: name,
        nutrientName: nutrient
      });
    }
  }
  return list;
}

const formatDescription = (list) => {
  const L = [];
  for (const item of list) {
    const { materialName, nutrientName, nutrientValue, nutrientUnit, message } = item;
    if (message) {
      continue;
    }
    L.push(`${materialName}的${nutrientName}含量是${nutrientValue}${nutrientUnit}`)
  }
  const content = `查询到${L.join('，')}`;
  return content;
}

const invokeSearchNutrition = async (intent, options) => {
  const message = validateOptions(options, rules)
  if (message) { return message }
  // 调用接口查询营养指标
  const uri = '/feedgpt/api/WjChat/getMaterialInfo/1';
  const data = resolveRequestOptions(options);
  const info = await httpInvoke('POST', uri, data);
  // console.log("info", info);
  if (!info.success) {
    return info.message;
  }
  const list = info.result;
  const content = formatDescription(list);
  return content;
  // return '开始调用';
}

module.exports = exports = invokeSearchNutrition;