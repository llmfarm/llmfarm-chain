const httpInvoke = require('../common/invoke')
const MarkdownTable = require('../common/MarkdownTable');


const resolveRequestOptions = (options) => {
  const { nutrientName = [], value = [] } = options;
  const list = [];
  let i = 0;
  for (const name of nutrientName) {
    const adjustValue = value[i];
    calculationType = adjustValue >= 0 ? 1 : 2;
    list.push({
      nutrientName: name,
      calculationType,
      value: Math.abs(adjustValue)
    });
    i++;
  }
  return list;
}

const invokeAdjustNutrition = async (intent, options) => {

  const data = resolveRequestOptions(options);
  const uri = '/feedgpt/api/WjChat/adjustmentRecipe/1';
  const info = await httpInvoke('POST', uri, data);
  if (!info.success) {
    return info.message;
  }
  const list = info.result;

  const headers = ['序号', '营养素名称', '含量', '单位'];
  const markdownTable = new MarkdownTable(list, headers, true).asMarkdownTable();

  return `好的, 30kg体重的生长猪调整后的营养需要量如下:
  
${markdownTable}

请问是否需要调整?`
}

module.exports = exports = invokeAdjustNutrition;