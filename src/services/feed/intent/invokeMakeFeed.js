const httpInvoke = require('../common/invoke')
const MarkdownTable = require('../common/MarkdownTable');

const invokeMakeFeed = async (intent, options) => {

  const uri = '/feedgpt/api/WjChat/getAnimalRecipeResult/1';
  const info = await httpInvoke('GET', uri);
  const result = info.result || {};
  console.log(JSON.stringify(result, null, 2));
  // return result.toString();
  const list = result.animalRecipeResultVOList || []
  const nutritionList = result.animalRecipeInfoVOList || [];

  const materialHeaders = ['序号', '原料', '占比'];
  const materialTable = new MarkdownTable(list, materialHeaders, true).asMarkdownTable();

  const nutritionHeaders = ['序号', '营养素', '含量', '单位'];
  const nutritionTable = new MarkdownTable(nutritionList, nutritionHeaders, true).asMarkdownTable();

  return `好的，30kg的生长育肥猪的饲料配方已制作完成，具体如下：
  
${materialTable}

${nutritionTable}`
}

module.exports = exports = invokeMakeFeed;