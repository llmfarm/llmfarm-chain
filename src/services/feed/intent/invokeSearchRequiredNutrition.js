// 查询营养需要量
const { validateOptions } = require('../common/validate')
const httpInvoke = require('../common/invoke');

const rules = {
  'stageName': {
    required: true,
    message: (options) => {
      if (options.stageName) return null;
      const message = `好的，请问您需要制作配方的猪处于什么阶段

可选项为: 仔猪、生长育肥猪、后备母猪、种公猪、妊娠母猪、泌乳母猪 
`
      return message
    }
  },
  'weight': {
    message: (options, key) => {
      const set = new Set(['仔猪', '生长育肥猪', '后备母猪', '种公猪']);
      if (!options[key] && set.has(options.stageName)) {
        const message = `好的，请问您需要制作配方的猪体重是多少公斤`
        return message
      }
      return '';
    }
  },
  'childbirth': {
    message: (options, key) => {
      const set = new Set(['妊娠母猪', '泌乳母猪']);
      if (!options[key] && set.has(options.stageName)) {
        const message = `好的，请问您需要制作配方的猪的分娩胎次是多少次`
        return message
      }
      return '';
    }
  }
}


const formatNutrition = list => {
  const tableHeader = '| 序号 | 营养素名称 | 含量 | 单位 |\n|------|------------|------|------|\n';
  let body = '';

  for (let i = 0; i < list.length; i++) {
    const item = list[i];
    const nutrientName = item.nutrientName;
    const nutrientValue = item.nutrientValue;
    const nutrientUnit = item.nutrientUnit;
    const row = `| ${i + 1} | ${nutrientName} | ${nutrientValue} | ${nutrientUnit} |\n`;
    body += row;
  }
  const markdownTable = tableHeader + body;
  return markdownTable;
}

const invokeSearchRequiredNutrition = async (intent, options) => {
  const validateInfo = validateOptions(options, rules)
  if (validateInfo) {
    return validateInfo;
  }

  const uri = "/feedgpt/api/WjChat/getAnimalRecipeInfo";
  const data = { userId: '1', ...options }
  const info = await httpInvoke('POST', uri, data);
  console.log("info", info);
  const list = info.result || [];
  const table = formatNutrition(list);

  const message = `好的, 查询到30kg体重的生长猪营养需要量如下: \n\n${table}
  
请问是否需要调整?`
  return message;
}

module.exports = exports = invokeSearchRequiredNutrition

// const run = async () => {
//   const options = {
//     weight: 20,
//     stageName: '仔猪',
//     childbirth: ''
//   }
//   const message = await invokeSearchRequiredNutrition(null, options);
//   console.log(message);
// }
// run();