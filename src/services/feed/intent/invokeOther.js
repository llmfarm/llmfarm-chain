const invokeOther = async (intent, options) => {
  const question = options.question;

  const content = `很抱歉，作为Feed AI助手，我无法回答“${question}”的问题。

您可以向我询问关于原料参数查询、猪饲料配方制作相关的问题。`
  return content;
}

module.exports = exports = invokeOther;