const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
const streaming = async (content, onTokenStream) => {
  for (const ch of content) {
    await delay(20);
    await onTokenStream(ch);
  }
  return content;
}

const invokeSearchNutrition = require('./intent/invokeSearchNutrition')
const invokeSearchRequiredNutrition = require('./intent/invokeSearchRequiredNutrition');
const invokeAdjustNutrition = require('./intent/invokeAdjustNutrition');
const invokeMakeFeed = require('./intent/invokeMakeFeed');
const invokeOther = require('./intent/invokeOther')

const invokePolite = async (intent, options) => {
  return `如果您还有其他问题，欢迎随时向我咨询。`
}

const hash = {
  'search_nutrition': invokeSearchNutrition, // 1. 查询<作物>的<营养指标>含量, 需要调用 API (查询原料的营养参数)
  'search_required_nutrition': invokeSearchRequiredNutrition, // 2. 查询[猪种类], [体重]/[胎次]的营养需求量
  'adjust_nutrition': invokeAdjustNutrition, // 3. 意图: 根据上文中提及的营养量需求, 调整配方含量
  'make_feed': invokeMakeFeed, // 4. 意图: 希望制作猪饲料配方, 只支持猪饲料
  'polite': invokePolite, // 5. 意图: 客气的话语、感谢等
  'other': invokeOther // 6. 意图: 其他无关意图
}

const invoke = async (intent, params, question, userId, onTokenStream) => {
  const fn = hash[intent];
  const options = Object.assign(params, { question, userId })
  if (fn) {
    const result = await fn(intent, options);
    // console.log('fn.result', result);
    return await streaming(result, onTokenStream);
  }
  return await streaming('请问还有什么可以帮助您的吗?', onTokenStream);
}

module.exports = exports = invoke;

const run = async () => {
  const onTokenStream = async (token) => {
    process.stdout.write(token)
  }
  // await invoke('search_nutrition', { '作物名称': ['玉米'], ['营养指标']: ['蛋白质'] }, '', 1, onTokenStream)
  // await invoke('other', {}, '今天的天气怎么样', 1, onTokenStream)
  // const info = { "intent": "search_required_nutrition", "params": { "stageName": '泌乳母猪', "childbirth": 2, } }
  // const info = { "intent": "make_feed" }
  // const info = { "intent": "adjust_nutrition", "params": { "nutrientName": ["消化能", "异亮氨酸"], "value": [1, -0.2] } }
  const info = {
    intent: 'search_nutrition',
    params: { 'materialName': ['玉米', '皮大麦'], 'nutrientName': ['粗蛋白', '脂肪'] }
  }
  await invoke(info.intent, info.params || {}, '', 1, onTokenStream)
}

// run();