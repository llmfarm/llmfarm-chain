const axios = require('axios');
require('dotenv').config();
const host = process.env.FEEDGPT_HOST;

const invoke = async (method, uri, data = {}) => {

  const url = `${host}${uri}`;
  if (method === 'GET') {
    const response = await axios.get(url, { params: data });
    return response.data;
  }

  if (method === 'POST') {
    const response = await axios.post(url, data);
    return response.data;
  }
}

module.exports = exports = invoke;