require('module-alias/register');
require('dotenv').config();
const NodeChatCompletion = require("@src/chainflow/executor/NodeChatCompletion")

const PromptBuilder = require('./PromptBuilder');
const builder = new PromptBuilder();
const invokeIntent = require('./invokeIntent');

const resolveJSON = (content) => {
  const reg = /```json([^`]*)```/gi;
  if (reg.test(content)) {
    const json = this.capture(content, reg);
    const o = JSON.parse(json);
    return o
  }
  try {
    const o = JSON.parse(content);
    return o;
  } catch (err) {
    console.log('JSON.parse error', err);
  }
  return content
};


const chat = async (question, messages = [], userId = 1, onTokenStream = () => { }) => {
  // await resolveMessages(messages);
  const context = await builder.build();
  console.log('context', context);
  const result = await NodeChatCompletion({ prompt: question, context }, {}, { messages }, onTokenStream);
  const json = resolveJSON(result);
  console.log('json', json);
  const { intent, params } = json;
  await invokeIntent(intent, params, question, userId, onTokenStream);
}

module.exports = exports = chat;


const run = async () => {
  const onTokenStream = (ch) => {
    process.stdout.write(ch);
  }
  await chat('请问玉米的蛋白质含量是多少', [], 1, onTokenStream);
}

// run();